<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Balance_Lib_Model_Std_AbstractArrayTraverse
{
    /**
	 * Get the nodes by a certain Path.
	 * e.g. 'Category/Product' => array('Category', 'Product').
	 * 
	 * @param string $path Path (in text) to convert (e.g. 'Category/SubCategory').
	 * @param string $pathDelimiter Path delimiter (e.g. '/', '-'...).
	 * @return array|boolean Array of nodes or false if the path is empty or empty-like ('/').
	 */
	public function getNodesByPath($path, $pathDelimiter = '/')
	{
		// An empth path ('') or empty-like path ('/','-',...) will be treated as false.
		$path = trim(trim($path), $pathDelimiter);
		if (empty($path))
		{
			return false;
		}
		$nodes = explode($pathDelimiter, $path);
		return $nodes;
	}

	/**
	 * Whether the given path exists in the tree.
	 * 
	 * @param string $path Path to check.
	 * @param string $pathDelimiter Path delimiter ('/', '-', ....).
	 * @return boolean|mixed False if path does not exist, or the child value of the path in the tree.
	 */
	public function isPathExists($path, $pathDelimiter = '/')
	{
		$nodes = $this->getNodesByPath($path, $pathDelimiter);
		if ($nodes === false)
		{
			return false;
		}
		else
		{
			return ($this->getChild($this->toArray(), $nodes) !== false);
		}
	}
	
	/**
	 * Recursive method to get the value (or a sub tree).
	 * 
	 * @param mixed $tree Value(s) of the tree (e.g. array('node12'=>array(....), 'node22'=>'val',...)).
	 * @param string|array $node Path(s) (in array) in the tree. Could be an array, e.g. array('category','subcategory',...), or a string 'category', or null.
	 * @return boolean|mixed False if the node does not exist in the tree, or the value of the node in the true.
	 */
	public function getChild($tree, $node)
	{
		if (!isset($node))
		{
			return false;
		}
		if (!is_array($tree))
		{
			return false;
		}
		if (is_array($node))
		{
			$currentNode = array_shift($node);
			if (!empty($node))
			{
				return $this->getChild($tree[$currentNode], $node);
			}
			else
			{
				return array_key_exists($currentNode, $tree) ? $tree[$currentNode] : false;				
			}
		}
		else
		{
			return array_key_exists($node, $tree) ? $tree[$node] : false;
		}
	}
	
	/**
	 * Get value based on a certain path in the tree.
	 * 
	 * @param string $path Path to check.
	 * @param string $pathDelimiter Path delimiter ('/', '-', ....). 
	 * @return mixed Return null if the path does not exist.
	 */
	public function getChildByPath($path, $pathDelimiter = '/')
	{
		$nodes = $this->getNodesByPath($path, $pathDelimiter);
		if ($nodes !== false)
		{
			$child = $this->getChild($this->toArray(), $nodes);
			if ($child !== false)
			{
				return $child;
			}
			return null;
		}
		return null;
	}
    
    abstract function toArray();
}
?>
