<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_Std_ArrayObject extends ArrayObject
{
    public function toArray($recursive = false)
    {
        if ($recursive)
        {
            // @todo
        }
        else
        {
            return $this->getArrayCopy();
        }
    }
    
    public function fromArray(array $array)
    {
        $this->exchangeArray($array);
    }
    
    public function __toString()
    {
        return $this->serialize();
    }
    
    public function toXml()
    {
        /**
         * @todo Conver an array to xml. 
         */
    }
    
    public function getFields()
    {
        return array_keys($this->getArrayCopy());
    }
}
?>
