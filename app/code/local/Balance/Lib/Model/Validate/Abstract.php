<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
abstract class Balance_Lib_Model_Validate_Abstract implements Balance_Lib_Model_Validate_Interface
{
    protected $_messages = array();
    
    protected $_options = array();
    
    public function __contruct($options = array())
    {
        if (isset($options))
        {
            $this->_options = $options;
        }
    }
    
    public function setOptions($options)
    {
        $this->_options = $options;
    }
    
    public function getOptions()
    {
        return $this->_options;
    }
    
    public function reset()
    {
        $this->_messages = array();
        $this->_options = array();
    }
    
    public function getMessages()
    {
        return $this->_messages;
    }
    
    public function addMessage($message)
    {
        $this->_messages[] = $message;
    }
    
    public function setMessages(array $messages)
    {
        $this->_messages = $messages;
    }
}
?>
