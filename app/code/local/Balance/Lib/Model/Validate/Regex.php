<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_Validate_Regex extends Balance_Lib_Model_Validate_Abstract
{
    public function isValid($value) 
    {
        $options = $this->getOptions();
        if (empty($options[0]))
        {
            // No pattern spcified. Aussme the value is valid.
            return true;
        }
        if (!preg_match($options[0], $value))
        {
            $this->addMessage("Given value '{$value}' does not match the given pattern '{$options[0]}'");
            return false;
        }
        return true;
    }
}
?>
