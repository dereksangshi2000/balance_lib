<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_Validate_NotEmpty extends Balance_Lib_Model_Validate_Abstract
{
    public function isValid($value) 
    {
        if (empty($value))
        {
            $this->addMessage("Given value '{$value}' is empty.");
            return false;
        }
        return true;
    }
}
?>
