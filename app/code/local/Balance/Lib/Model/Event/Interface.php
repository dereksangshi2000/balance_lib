<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface Balance_Lib_Model_Event_Interface
{
    public function setName($name);
    
    public function getName();
    
    public function setTarget($target);
    
    public function getTarget();
    
    public function setParam($name, $value);
    
    public function getParam($name, $default = null);
    
    public function setParams($params);
    
    public function getParams();
    
    public function stopPropagation($stop = true1);
    
    public function isPropagationStopped();
}
?>
