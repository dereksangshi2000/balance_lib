<?php

/**
 * Balance_Lib_Model_Event_Event class.
 * Concrate observer in observer pattern.
 * 
 * @see Balance_Lib_Model_Event_Interface
 * @author Derek Li
 */
class Balance_Lib_Model_Event_Event implements Balance_Lib_Model_Event_Interface
{
    protected $name = null;
    
    protected $target = null;
    
    protected $params = array();
    
    protected $isPropagationStopped = false;
    
    public function __construct($name = null, $target = null, $params = null)
    {
        if (isset($name))
        {
            $this->setName($name);
        }
        if (isset($target))
        {
            $this->setTarget($target);
        }
        if (isset($params))
        {
            $this->setParams($params);
        }
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setTarget($target)
    {
        $this->target = $target;
    }
    
    public function getTarget()
    {
        return $this->target;
    }
    
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
    }
    
    public function getParam($name, $default = null)
    {
        if (array_key_exists($name, $this->params))
        {
            return $this->params[$name];
        }
        else
        {
            return isset($default) ? $default : false;
        }
    }
    
    public function setParams($params)
    {
        $this->params = $params;
    }
    
    public function getParams()
    {
        return $this->params;
    }
    
    public function stopPropagation($stop = true)
    {
        $this->isPropagationStopped = $stop;
    }
    
    public function isPropagationStopped()
    {
        return $this->isPropagationStopped;
    }
}
?>