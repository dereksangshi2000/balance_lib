<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface Balance_Lib_Model_Event_Observer_Interface
{
    public function attach($event, $callback = null, $priority = 1);
    
    public function detach($observer);
    
    public function trigger($event, $target = null, $params = array());
    
    public function getEvents();
}
