<?php

/**
 * Balance_Lib_Model_Event_Observer class.
 * Concrate observer in observer pattern.
 * 
 * @see Balance_Lib_Model_Event_Observer_Interface
 * @author Derek Li (derek@balanceinternet.com.au)
 */
class Balance_Lib_Model_Event_Observer implements Balance_Lib_Model_Event_Observer_Interface
{
    /**
     * Events observed.
     * @var array
     */
    protected $_events = array();
    
    /**
     * Stopped events.
     * @var array 
     */
    protected $_stoppedEvents = array();
    
    /**
     * Attach a certain event.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Event name of the event instance.
     * @param mixed $callback OPTIONAL Callback function to call the event is triggered.
     * @param integer $priority OPTIONAL Priority of the callback function for the event.
     * @return Balance_Lib_Model_Event_Observer
     */
    public function attach($event, $callback = null, $priority = 1)
    {
        // Not a recognized event.
        if ($this->getEventName($event) === false)
        {
            /**
             * @todo throw exception (Given event not recognized).
             */
            return $this;
        }
        if (!$this->hasEvent($event))
        {
            $this->_events[$this->getEventName($event)]['event'] = $event;
            $this->_events[$this->getEventName($event)]['callbacks'] = array();
        }
        if (!isset($callback))
        {
            return $this;
        }
        if (isset($priority))
        {
            $this->_events[$this->getEventName($event)]['callbacks'][$priority] = $callback;
        }
        else
        {
            $this->_events[$this->getEventName($event)]['callbacks'][] = $callback;
        }
        return $this;
    }
    
    /**
     * Detach a certain event.
     * 
     * @param string $event Event to detach.
     * @param mixed $callback OPTIONAL Callback to detach for the event.
     * @return Balance_Lib_Model_Event_Observer
     */
    public function detach($event, $callback = null)
    {
        if ($this->hasEvent($event))
        {
            if (isset($callback))
            {
                /**
                 * @todo remove spcified callback for the given event. 
                 */
            }
            else
            {
                $this->removeEvent($event);
            }
        }
        return $this;
    }
    
    /**
     * Trigger the event.
     * 
     * @param string $event Event to trigger.
     * @param mixed $target OPTIONAL Target which triggers the event.
     * @param array $params OPTIONAL Params to pass.
     * @return boolean|Balance_Lib_Model_Event_Observer
     */
    public function trigger($event, $target = null, $params = null)
    {
        if (!$this->hasEvent($event) || $this->isEventStopped($event))
        {
            return false;
        }
        $event = $this->getEvent($event);
        if (is_string($event))
        {
            $event = new Balance_Lib_Model_Event_Event($event);
        }
        if (!($event instanceof Balance_Lib_Model_Event_Interface))
        {
            return $this;
        }
        if (isset($target))
        {
            $event->setTarget($target);
        }
        // Merge params to event (NOTICE: the given params will be set to event individually, so it's actually a merge, not a replace).
        if (isset($params))
        {
            if (!is_array($params))
            {
                $params = array($params);
            }
            foreach ($params as $name => $value)
            {
                $event->setParam($name, $value);
            }
        }
        // Run binded callbacks.
        $callbacks = $this->getCallbacks($event);
        if (is_array($callbacks) && count($callbacks) > 0)
        {
            // Sort callbacks.
            ksort($callbacks);
            foreach ($callbacks as $callback)
            {
                if ($event->isPropagationStopped())
                {
                    break;
                }
                $this->_triggerCallback($event, $callback);
            }
        }
        return $this;
    }
    
    /**
     * Trigger the callback method.
     * 
     * @param Balance_Lib_Model_Event_Interface $event Event triggered.
     * @param mixed $callback Callback to run.
     */
    protected function _triggerCallback(Balance_Lib_Model_Event_Interface $event, $callback)
    {
        if (is_string($callback))
        {
            $callback = array($callback, 'observe');
        }
        if (is_array($callback))
        {
            $instance = $callback[0];
            if (is_string($instance))
            {
                $instance = Balance_Lib_Model_Di_Container::getInstance()->get($instance);
            }
            if (is_object($instance) && isset($callback[1]) && method_exists($instance, $callback[1]))
            {
                $instance->{$callback[1]}($event);
            }
        }
    }
    
    /**
     * Get all the attached events (with callbacks).
     * 
     * @return array
     */
    public function getEvents()
    {
        return $this->_events;
    }
    
    /**
     * Stop the event.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     */
    public function stopEvent($event)
    {
        if ($this->hasEvent($event))
        {
            $this->_stoppedEvents[] = $this->getEventName($event);
        }
    }
    
    /**
     * Evaluate the event and return the event name if valid.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     * @return string|false Event name of the event is valid or false if the event not valid.
     */
    public function getEventName($event)
    {
        return is_string($event) ? $event : (($event instanceof Balance_Lib_Model_Event_Interface) ? $event->getName() : false);
    }
    
    /**
     * Resume the suspended events.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     */
    public function resumeEvent($event)
    {
        if ($this->hasEvent($event) && in_array($this->getEventName($event), $this->_stoppedEvents))
        {
            unset($this->_stoppedEvents[array_search($this->getEventName($event), $this->_stoppedEvents)]);
        }
    }
    
    /**
     * Get binded event.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     * @return array|null 
     */
    public function getEvent($event)
    {
        if ($this->hasEvent($event))
        {
            return $this->_events[$this->getEventName($event)]['event'];
        }
        return null;
    }
    
    public function getCallbacks($event)
    {
        if ($this->hasEvent($event))
        {
            return $this->_events[$this->getEventName($event)]['callbacks'];
        }
        return null;
    }
    
    /**
     * Check if the event has been stopped.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     * @return boolean True if stopped, or false otherwise. 
     */
    public function isEventStopped($event)
    {
        return in_array($this->getEventName($event), $this->_stoppedEvents);
    }
    
    /**
     * Remove the event.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     */
    public function removeEvent($event)
    {
        if ($this->hasEvent($event))
        {
            unset($this->_events[$this->getEventName($event)]);
        }
    }
    
    /**
     * Check if the event exists.
     * 
     * @param string|Balance_Lib_Model_Event_Interface $event Name or instance of the event.
     * @return boolean True if the event exists, or false otherwise. 
     */
    public function hasEvent($event)
    {
        return array_key_exists($this->getEventName($event), $this->_events);
    }
}
?>