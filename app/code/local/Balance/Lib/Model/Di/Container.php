<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_Di_Container
{
    protected $_instances = array();
    
    protected $_definitionList = null;
    
    protected $_instanceManager = null;
    
    public function __construct() 
    {
        
    }
    
    public function setDefinitionList($definitionList)
    {
        $this->_definitionList = $definitionList;
    }
    
    public function getDefinitionList()
    {
        return $this->_definitionList;
    }
    
    public function setInstance($instanceManager)
    {
        $this->_instanceManager = $instanceManager;
    }
    
    /**
     *
     * @return Balance_Lib_Model_Di_instanceManager
     */
    public function getInstanceManager()
    {
        if (!isset($this->_instanceManager))
        {
            $this->_instanceManager = new Balance_Lib_Model_Di_instanceManager();
        }
        return $this->_instanceManager;
    }
   
    
    public function get($class, $params = array())
    {
        $instanceKey = $this->getInstanceKey($class, $params);
        if (!$this->instanceExists($instanceKey))
        {
//             $class, $params, $this->getDefinitionList()
            $instanceManager = $this->getInstanceManager();
            $instanceManager->setClass($class);
            $instanceManager->setParams($params);
            $instanceManager->setDefinitionList($this->getDefinitionList());
            $this->addInstance($instanceManager->createInstance(), $instanceKey);
        }
        return $this->_instances[$instanceKey];
    }
    
    public function getInstanceKey($class, $params = array())
    {
        $key = array();
        $key['class'] = $class;
        if (!empty($params))
        {
            $key['params'] = $params;
        }
        return serialize($key);
    }
    
    public function removeInstance($instanceKey)
    {
        if ($this->instanceExists($instanceKey))
        {
            unset($this->_instances[$instanceKey]);
        }
    }
    
    public function addInstance($instance, $instanceKey)
    {
        $this->_setInstance($instanceKey, $instance);
    }
    
    protected function _setInstance($instanceKey, $instance)
    {
        $this->_instances[$instanceKey] = $instance;
    }
    
    public function instanceExists($instanceKey)
    {
        return array_key_exists($instanceKey, $this->_instances) && is_object($this->_instances[$instanceKey]);
    }
    
    public function newInstance($class, $params = array())
    {
        $instanceKey = $this->getInstanceKey($class, $params);
        if ($this->instanceExists($instanceKey))
        {
            $this->removeInstance($instanceKey);
        }
        return $this->get($class, $params);
    }
    
    /**
     * Use magento singleton handler.
     * 
     * @return Balance_Lib_Model_Di 
     */
    public static function getInstance()
    {
        return Mage::getSingleton('balance_lib/di_container');
    }
}
?>
