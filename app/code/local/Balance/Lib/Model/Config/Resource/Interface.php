<?php

/**
 * Config source interface.
 * 
 * @author Derek Li
 */
interface Balance_Lib_Model_Config_Resource_Interface
{
    /**
     * Read the resource. 
     */
    public function read();
    
    /**
     * Write the resource. 
     */
    public function write();
    
    /**
     * Convert the content of the resource into an array and return the array.
     * 
     * @return array
     */
    public function toArray();
}
?>
