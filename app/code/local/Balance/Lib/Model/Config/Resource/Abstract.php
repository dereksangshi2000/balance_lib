<?php

/**
 * Balance_Lib_Model_Config_Resource_Abstract class.
 * 
 * @author Derek Li
 */
abstract class Balance_Lib_Model_Config_Resource_Abstract implements Balance_Lib_Model_Config_Resource_Interface
{
    /**
     * The resource (e.g. the filename of the resource, the link to the resource.....)
     * 
     * @var mixed
     */
    protected $_resource = null;
    
    /**
     * The content of the resource in an array.
     * 
     * @var array 
     */
    protected $_data = null;
    
    /**
     * Options.
     * 
     * @var array 
     */
    protected $_options = array();
    
    /**
     * Constructor.
     * 
     * @param mixed $resource OPTIONAL The resource.
     * @param mixed $options OPTIONAL Options needed.
     */
    public function __construct($resource = null, $options = array())
    {
        if (isset($resource))
        {
            $this->setResource($resource);
        }
        if (isset($options))
        {
            $this->setOptions($options);
        }
    }
    
    /**
     * Set options.
     * 
     * @param mixed $options The options to set.
     */
    public function setOptions($options)
    {
        if (is_array($options))
        {
            foreach ($options as $name => $val)
            {
                // Use the set method (if exists) to set the option.
                $methodName = 'set'.$this->camelize($name);
                if (method_exists($this, $methodName))
                {
                    $this->{$methodName}($val);
                }
                // Set the option by default to the instance variable.
                else
                {
                    $this->_options[$name] = $val;
                }
            }
        }
    }
    
    /**
     * Get a certain option.
     * 
     * @param string $name Name of the option.
     * @return mixed The value of the option. 
     */
    public function getOption($name)
    {
        // Get the value of the option using the get method (if exists).
        $methodName = 'get'.$this->camelize($name);
        if (method_exists($this, $methodName))
        {
            return $this->{$methodName}();
        }
        // Get the value of the option from the instance variable (if exists).
        if (array_key_exists($name, $this->_options))
        {
            return $this->_options[$name];
        }
        return null;
    }
    
    /**
     * Get all the options.
     * 
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }
    
    /**
     * Camelize the name. (e.g. aaa_bbs => AaaBbs)
     * 
     * @param string $name Name to get camelized.
     * @param string $delimiter OPTIONAL The delimiter used in the name.
     * @return string 
     */
    protected function camelize($name, $delimiter = '-')
    {
        // Convert option name to method name. (e.g. aaaa_bbb => AaaaBbb, aa => Aa, aa_bcd_cvgbw => AaBcdCvgbw)
        return str_replace(' ', '', ucwords(str_replace($delimiter, ' ', $name)));
    }
    
    /**
     * Set the resource.
     * 
     * @param string $resource The resource.
     */
    public function setResource($resource)
    {
        $this->_resource = $resource;
    }
    
    /**
     * Get the resource.
     * 
     * @return string 
     */
    public function getResource()
    {
        return $this->_resource;
    }
    
    /**
     * If $this has a resource.
     * 
     */
    public function hasResource()
    {
        return !empty($this->_resource);
    }
    
    public function read()
    {
        
    }
    
    public function write()
    {
        
    }
    
    /**
     * Read the content of the resource and put the content into an array.
     * 
     * @return array
     */
    public function toArray()
    {
        if (!isset($this->_data))
        {
            $this->read();
        }
        return $this->_data;
    }
}
?>
