<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_Config_Resource_Yaml_Zend extends Balance_Lib_Model_Config_Resource_Abstract
{
    public function read()
    {
        $config = new Zend_Config_Yaml($this->getResource(), $this->getOption('env'));
        $this->_data = $config->toArray();
    }
    
    public function write()
    {
        
    }
}
?>
