<?php
/**
 * Balance_Lib_Model_Config class.
 * A config can have more than one resource.
 * The config can be managed by a config manager once it's been registerd to.
 * 
 * @author Derek Li
 */
abstract class Balance_Lib_Model_Config_Abstract extends Balance_Lib_Model_Std_AbstractArrayTraverse
{
    /**
     * Name of the config.
     * This value will be used as the identity of the config (which will be used by config manager).
     * 
     * @var string
     */
    protected $_name = null;
    
    /**
     * Resources hold.
     * 
     * @var array 
     */
    protected $_resources = null;
    
    /**
     * Data from the resources.
     * 
     * @var array 
     */
    protected $_data = array();
    
    /**
     * Constructor.
     * 
     * @param string $name OPTIONAL Name of the config.
     */
    public function __construct($name = null)
    {
        if (isset($name))
        {
            $this->setName($name);
        }
        $this->_init();
    }
    
    protected function _init()
    {
        
    }
    
    /**
     * Set name of the config.
     * 
     * @param string $name Name to set.
     */
    public function setName($name)
    {
        $this->_name = $name;
    }
    
    /**
     * Get name of the config.
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }
    
    /**
     * Register this config to its manager.
     * 
     * @param ConfigManager $manager The config manager to register to.
     */
    public function registerTo($manager)
    {
        if ($manager instanceof ConfigManager)
        {
            $manager->addConfig($this);
        }
    }
    
    /**
     * Add resource to the config.
     * 
     */
    public function addResource($resource, $priority = null)
    {
        if (!($resource instanceof Balance_Lib_Model_Config_Resource_Interface))
        {
            /**
             * @todo throw exception. 
             */
            return;
        }
        if (!isset($priority))
        {
            $this->_resources[] = $resource;
        }
        else
        {
            $this->_resources[$priority] = $resource;
        }
    }
    
    public function getResources()
    {
        return $this->_resources;
    }
    
    /**
     * Merge all the resources.
     * If there are same fields in two resources, the latter one will replace the former one.
     * 
     */
    public function merge()
    {
        ksort($this->_resources);
        foreach ($this->_resources as $res)
        {
            $this->_data = $this->mergeRecursiveDistinct($this->_data, $res->toArray());
        }
    }
    
    /**
     * Merge two arrays recursively and distinctly.
     * 
     * @see http://www.php.net/manual/en/function.array-merge-recursive.php
     * @param array $array1 Initial array to merge.
     * @param array $array2 Array to recursively merge.
     * @return array Merged array
     */
    public function mergeRecursiveDistinct(array $array1, array $array2)
    {
        $merged = $array1;
        foreach ($array2 as $key => $value)
        {
            if (is_array($value ) && isset($merged[$key]) && is_array($merged[$key]))
            {
                $merged[$key] = $this->mergeRecursiveDistinct($merged[$key], $value);
            }
            else
            {
                $merged[$key] = $value;
            }
        }
        return $merged;
    }
    
    /**
     * Merge all the resources currently currently sitting inside and return the result as an array.
     * 
     * @return array 
     */
    public function toArray()
    {
        if (empty($this->_data))
        {
            $this->merge();
        }
        return $this->_data;
    }
}
?>
