<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
abstract class Balance_Lib_Model_AMQ_Publisher_Abstract
{
    /**
     * The frame to use.
     * 
     * @var Balance_Lib_Model_AMQ_Transport_Frame_Interface 
     */
    protected $_frame = null;
    
    /**
     * Construct.
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Frame_Interface $frame OPTIONAL The frame to use.
     */
    public function __construct($frame = null)
    {
        if (isset($frame) && ($frame instanceof Balance_Lib_Model_AMQ_Transport_Frame_Interface))
        {
            $this->setFrame($frame);
        }
    }
    
    /**
     * Set the frame.
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Frame_Interface $frame The frame to use.
     */
    public function setFrame(Balance_Lib_Model_AMQ_Transport_Frame_Interface $frame)
    {
        $this->_frame = $frame;
    }
    
    /**
     * Get the default frame.
     * 
     * @return Balance_Lib_Model_AMQ_Transport_Frame_Internal 
     */
    protected function _getDefaultFrame()
    {
        return Balance_Lib_Model_AMQ_Di::getInstance()->newInstance('transport_frame_internal');
    }
    
    /**
     * Get the frame.
     * 
     * @return Balance_Lib_Model_AMQ_Transport_Frame_Interface 
     */
    public function getFrame()
    {
        // Use default frame if not set.
        if (!isset($this->_frame))
        {
            $this->_frame = $this->_getDefaultFrame();
        }
        return $this->_frame;
    }
}
?>

