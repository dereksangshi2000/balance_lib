<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Exchange_Direct extends Balance_Lib_Model_AMQ_Exchange_Abstract
{
    /**
     * The message is matched with the queue if the routing key is equal to the binding.
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Message_Interface $message
     * @param Balance_Lib_Model_AMQ_Queue_Interface $queue
     * @return boolean
     */
    public function match($message, $queue)
    {
        // Get binding and use name of the queue as binding if not specified.
        $binding = $this->getBinding($queue->getName());
        if (!isset($binding))
        {
            $binding = $queue->getName();
        }
        // Route the message to the queue if the routing of the message equals to the binding(or name) of the queue.
        if ($message->getRoutingKey() == $binding)
        {
            return true;
        }
        return false;
    }
    
    public function isBindingValid($binding) 
    {
        return true;
    }
}
?>
