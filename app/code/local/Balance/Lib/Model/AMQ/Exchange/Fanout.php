<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Exchange_Fanout extends Balance_Lib_Model_AMQ_Exchange_Abstract
{
    /**
     * Every message is matched by each queue.
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Message_Interface $message
     * @param Balance_Lib_Model_AMQ_Queue_Interface $queue
     * @return boolean
     */
    public function match($message, $queue)
    {
        return true;
    }
    
    public function isBindingValid($binding) 
    {
        return true;
    }
}
?>
