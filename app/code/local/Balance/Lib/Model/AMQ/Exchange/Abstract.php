<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
abstract class Balance_Lib_Model_AMQ_Exchange_Abstract implements Balance_Lib_Model_AMQ_Exchange_Interface
{
    protected $_queues = array();
    
    protected $_bindings = array();
    
    protected $_messages = array();
    
    public function bind($queue, $binding = null)
    {
        if ($queue instanceof Balance_Lib_Model_AMQ_Queue_Interface)
        {
            if (!$this->isBindingValid($binding))
            {
                /**
                 * @todo Throw exception (incorrect binding).
                 */
                return $this;
                
            }
            $this->_queues[$queue->getName()] = $queue;
            $this->_bindings[$queue->getName()] = $binding;
        }
        return $this;
    }
    
    /**
     * Check if the binding is valid.
     * 
     * @param mixed $binding The binding of the queue.
     */
    abstract function isBindingValid($binding);
    
    public function accept($message)
    {
        if (!($message instanceof Balance_Lib_Model_AMQ_Transport_Message_Interface))
        {
            /**
             * @todo Should throw exception (message not a instance of Balance_Lib_Model_AMQ_Transport_Message_Interface).
             */
            return $this;
        }
        if ($message->getRoutingKey() == null || $message->getRoutingKey() == '')
        {
            /**
             * @todo should thow exception (message has no routing key).
             */
            return $this;
        }
        $this->_messages[] = $message;
        return $this;
    }
    
    public function getQueues()
    {
        return $this->_queues;
    }
    
    public function getQueue($name)
    {
        if (array_key_exists($name, $this->_queues))
        {
            return $this->_queues[$name];
        }
    }
    
    public function getBindings()
    {
        return $this->_bindings;
    }
    
    public function getBinding($queueName)
    {
        if (array_key_exists($queueName, $this->_bindings))
        {
            return $this->_bindings[$queueName];
        }
    }
    
    public function getMessages()
    {
        return $this->_messages;
    }
    
    public function hasQueue()
    {
        if (is_array($this->getQueues()) && count($this->getQueues()) > 0)
        {
            return true;
        }
        return false;
    }
    
    public function hasMessage()
    {
        if (is_array($this->getMessages()) && count($this->getMessages()) > 0)
        {
            return true;
        }
        return false;
    }
    
    public function route()
    {
        if (!$this->hasMessage() || !$this->hasQueue())
        {
            return $this;
        }
        $messages = $this->getMessages();
        $queues = $this->getQueues();
        foreach ($messages as $message)
        {
            foreach ($queues as $queue)
            {
                if ($this->match($message, $queue))
                {
                    $queue->push($message);
                }
            }
        }
    }
    
    /**
     * Check if the message mataches with the queue.
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Message_Interface $message
     * @param Balance_Lib_Model_AMQ_Queue_Interface $queue
     * @return boolean 
     */
    abstract public function match($message, $queue);
}
?>
