<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface Balance_Lib_Model_AMQ_Exchange_Interface
{
    public function bind($queue, $args = null);
    
    public function accept($message);
    
    public function route();
}
?>
