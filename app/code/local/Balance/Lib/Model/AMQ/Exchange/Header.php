<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Exchange_Header extends Balance_Lib_Model_AMQ_Exchange_Abstract
{
    protected $_realBindingCache = array();
    
    /**
     * Queues are bound to this exchange with a table of arguments containing headers and values (optional). 
     * A special argument named "x-match" determines the matching algorithm, 
     * where "all" implies an AND (all pairs must match) 
     * and "any" implies OR (at least one pair must match). 
     * 
     * @see http://4.bp.blogspot.com/_HYd_TJzpt80/RyYRYpcd8rI/AAAAAAAAABM/B57Anfd4Bkg/s1600-h/amq_match.jpg
     * @param Balance_Lib_Model_AMQ_Transport_Message_Interface $message
     * @param Balance_Lib_Model_AMQ_Queue_Interface $queue
     * @return boolean
     */
    public function match($message, $queue)
    {
        $routingKey = $this->realBindingFromString($message->getRoutingKey());
        /**
         * Routing key is in invalid format (can not be converted).
         * A valid routing key should be something like "a=b,c=d".
         */
        if (count($routingKey) === 0)
        {
            /**
             * @todo Should throw exception (routing key invliad)
             */
            return false;
        }
        // Make sure the real binding of the queue will only by checked and converted once.
        if (!$this->isRealBindingCached($queue))
        {
            $this->cacheRealBinding($queue);
        }
        $binding = $this->getCachedRealBinding($queue);
        // The binding is actually meaningless (only contains 'x-match')
        if (count($binding) == 1)
        {
            return true;
        }
        $xMatch = $binding['x-match'];
        unset($binding['x-match']);
        switch ($xMatch)
        {
            case 'all': // Each set should be matched (e.g. 'a=b,c=d' matches 'a=b,c=d')
            {
                if (count($xMatch) != count($routingKey))
                {
                    return false;
                }
                // Not matched if there is one set from queue can not be found in the message routing key.
                foreach ($binding as $key => $val)
                {
                    if (array_key_exists($key, $routingKey) || $routingKey[$key] != $val)
                    {
                        return false;
                    }
                }
                return true;
            }
            case 'any'; // Only need to match one set (e.g. 'a=b,c=d' matches 'a=b,d=e,...')
            {
                // Matched if there is one set from queue can be found in the message routing key.
                foreach ($binding as $key => $val)
                {
                    if (array_key_exists($key, $routingKey) && $routingKey[$key] != $val)
                    {
                        return true;
                    }
                }
                return false;
            }
            default:
            {
                return false;
            }
        }
        
    }
    
    protected function isRealBindingCached($queue)
    {
        return array_key_exists($queue->getName(), $this->_bindingCache);
    }
    
    public function isBindingValid($binding) 
    {
        if (!isset($binding))
        {
            return false;
        }
        if (is_string($binding))
        {
            return preg_match('/x-match=(all|any)/', $binding);
        }
        else if (is_array($binding))
        {
            return array_key_exists('x-match', $binding);
        }
        return false;
    }
    
    public function cacheRealBinding($queue, $redo = false)
    {
        if ($this->isBindingCached($queue) && !$redo)
        {
            return $this;
        }
        $binding = $this->getBinding($queue);
        /**
         * If provided binding is a string, convert it to an array.
         * The provided binding could be an array (considered as 'real binding').
         */
        if (is_string($binding))
        {
            $binding = $this->realBindingFromString($binding);
        }
        $this->_realBindingCache[$queue->getName()] = $binding;
        return $this;
    }
    
    /**
     * Convert string to the real binding (an array).
     * 'a=b,c=d,x-match=all' => array('a'=>'b', 'c'=>'d', 'x-match'=>'all');
     * 
     * @param string $bindingString The binding string to convert.
     * @return array
     */
    public function realBindingFromString($bindingString)
    {
        $sets = explode(',', trim($bindingString));
        $realBinding = array();
        $pattern = '/(?P<key>.+)=(?P<value>.+)/';
        foreach ($sets as $set)
        {
            if (preg_match($pattern, $set, $matches))
            {
                $realBinding[$matches['key']] = $matches['value'];
            }
        }
        return $realBinding;
    }
    
    public function getCachedRealBinding($queue)
    {
        if ($this->isBindingCached($queue))
        {
            return $this->_bindingCache[$queue->getName()];
        }
    }
}
?>