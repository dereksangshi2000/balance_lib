<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Exchange_Topic extends Balance_Lib_Model_AMQ_Exchange_Abstract
{
    /**
     * This is not the normal Topic exchange, 
     * actually it uses PHP regular expression as the matching method. 
     * 
     * @param Balance_Lib_Model_AMQ_Transport_Message_Interface $message
     * @param Balance_Lib_Model_AMQ_Queue_Interface $queue
     * @return boolean
     */
    public function match($message, $queue)
    {
        // Get binding and use name of the queue as binding if not specified.
        $binding = $this->getBinding($queue->getName());
        return preg_match($binding, $message->getRoutingKey());
    }
    
    public function isBindingValid($binding) 
    {
        return (isset($binding) && is_string($binding));
    }
}
?>
