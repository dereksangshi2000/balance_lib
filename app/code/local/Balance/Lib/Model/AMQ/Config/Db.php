<?php
/**
 * Balance_Lib_Model_AMQ_Config_Db class.
 * 
 * @author Derek Li
 */
class Balance_Lib_Model_AMQ_Config_Db extends Balance_Lib_Model_Config_Resource_Abstract
{
    protected $_name = 'db';
    
    /**
     * Database connections.
     * Array of db connection instances (Zend_Db_Adapter_Pdo_Abstract).
     * 
     * @var array 
     */
    protected $_dbConnections = array();
    
    protected function _init()
    {
        $this->addResource(new Balance_Lib_Model_Config_Resource_Yaml_Zend(__DIR__.'/../../configs/db.yml', array('env'=>'production')));
    }
    
    /**
     * Get database details.
     * 
     * @param string $dbName Name of the db.
     * @param null|string $mode OPTIONAL Resource of the db (read or write).
     * @return array 
     */
    public function getDbDetails($dbName, $mode = 'r')
    {
        $mode = $this->getRealMode($mode);
        return $this->getChildByPath("/database/adapters/{$mode}/{$dbName}");
    }
    
    /**
     * Get the real mode needed.
     * Basically the method converts any given mode to the real valid mode ('read' or 'write')
     * 
     * @param string $mode OPTIONAL Given mode.
     * @return string
     */
    public function getRealMode($mode = 'r')
    {
        $mode = strtolower($mode);
        // Default 'write' if not 'read' or 'r' specified
        $mode = ($mode == 'read' || $mode == 'r') ? 'read' : 'write';
        return $mode;
    }
    
    /**
     * Get db connection.
     * Lazy load db connection instance.
     * 
     * @param string $dbName Name of the db.
     * @param null|string $mode OPTIONAL Resource of the db (read or write).
     * @return Zend_Db_Adapter_Pdo_Abstract|null
     */
    public function getDbConnection($dbName, $mode = 'r')
    {
        $mode = $this->getRealMode($mode);
        // Lazy load db connection.
        if (!(isset($this->_dbConnections[$mode]) && isset($this->_dbConnections[$mode][$dbName])))
        {
            return $this->_setupDbConnection($dbName, $mode);
        }
        else
        {
            return $this->_dbConnections[$mode][$dbName];
        }
    }
    
    /**
     * Set up db connection based on the config file and given db name and mode..
     * 
     * @param string $dbName Name of the database.
     * @param string $mode Name of the mode (read or write).
     * @return Zend_Db_Adapter_Pdo_Abstract|null
     */
    protected function _setupDbConnection($dbName, $mode)
    {
        $path = "/database/adapters/{$mode}/{$dbName}";
        $data = $this->getChildByPath($path);
        if (!empty($data))
        {
            $this->_dbConnections[$mode][$dbName] = Zend_Db::factory($data['adapter'], $data['options']);
        }
        return $this->_dbConnections[$mode][$dbName];
    }
}
?>
