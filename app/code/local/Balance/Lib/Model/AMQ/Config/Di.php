<?php
/**
 * Balance_Lib_Model_AMQ_Config_Db class.
 * 
 * @author Derek Li
 */
class Balance_Lib_Model_AMQ_Config_Di extends Balance_Lib_Model_Config_Resource_Abstract
{
    protected $_name = 'di';
    
    protected function _init()
    {
        $this->addResource(new Balance_Lib_Model_Config_Resource_Yaml_Zend(__DIR__.'/../../configs/di.yml', array('env'=>'production')));
    }
    
    public function getInstance()
    {
        $InstanceConfig = $this->getChildByPath("/di/instance_manager");
        $class = $InstanceConfig['class'];
        if (class_exists($class, true))
        {
            return new $class();
        }
    }
}
?>
