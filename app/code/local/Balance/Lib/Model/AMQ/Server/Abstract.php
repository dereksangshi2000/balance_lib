<?php

/**
 * Balance_Lib_Model_AMQ_Server_Abstract abstract class.
 * This the standrad server for AMQ core functions.
 * It works as frontend controller in the normal MVC.
 * The broker works as 'controller', and the action method in each broker works as 'actioin'.
 * 
 * @author Derek Li (derek@balanceinternet.com.au)
 */
abstract class Balance_Lib_Model_AMQ_Server_Abstract
{
    /**
     * Configuration of the server.
     * 
     * @var array
     */
    protected $_config = array();
    
    /**
     * Brokers the server holds.
     * 
     * @var array 
     */
    protected $_brokers = array();
    
    /**
     * The request path.
     * e.g. 'product/addProducts' (this means use the product broker and call the addProductAction method in that broker class).
     * 
     * @var string 
     */
    protected $_path = null;
    
    /**
     * Params passed to the broker action called.
     * 
     * @var array 
     */
    protected $_params = array();
    
    /**
     * Constructor.
     * 
     * @param arra|null $config OPTIONAL The config for the server.
     */
    public function __construct($config = null)
    {
        if (isset($config))
        {
            $this->setConfig($config);
        }
        $this->_init();
    }
    
    protected function _init()
    {
        
    }
    
    public function setConfig($config)
    {
        $this->_config = $config;
    }
    
    public function getConfig()
    {
        return $this->_config;
    }
    
    public function setPath($path)
    {
        $this->_path = $path;
    }
    
    public function getPath()
    {
        return $this->_path;
    }
    
    public function setParams($params)
    {
        $this->_params = $params;
    }
    
    public function getParams()
    {
        return $this->_params;
    }
    
    /**
     * Lazy load and return the broker based on the given name.
     * 
     * @param string $brokerName Name of the broker.
     * @return null|Balance_Lib_Model_AMQ_Broker_Interface
     */
    public function getBroker($brokerName)
    {
        if (!array_key_exists($brokerName, $this->_brokers))
        {
            $this->_loadBroker($brokerName);
        }
        if (!isset($this->_brokers[$brokerName]) || !($this->_brokers[$brokerName] instanceof Balance_Lib_Model_AMQ_Broker_Interface))
        {
            /**
             * @todo Throw Exception (Broker $brokerName not found.)
             */
            return null;
        }
        return $this->_brokers[$brokerName];
    }
    
    /**
     * Lazy load broker based on the given name.
     * 
     * @param string $brokerName Name of the broker to load.
     */
    protected function _loadBroker($brokerName)
    {
        
    }
    
    /**
     * Run the request.
     * 
     * @param string $path The request path.
     * @param array $params Params to pass to the request path.
     * @return null|array Return the reponse from the broker or null if the broker not found. 
     */
    public function run($path = null, $params = null)
    {
        $path = isset($path) ? $path : $this->getPath();
        $path = trim($path, '\/');
        $params = isset($params) ? $params : $this->getParams();
        
        $pathPattern = '#(.+)/(.+)#';
        if (!preg_match($pathPattern, $path, $matches))
        {
            /**
             * @todo Throw exception. 
             */
            return;
        }
        
        /**
         * $matches should look like this:
         * Array
         * (
         *    [0] => product/addProducts
         *    [1] => product
         *    [2] => addProducts
         * ) 
         */
        $brokerName = $matches[1];
        if (!isset($brokerName))
        {
            /**
             * @todo Throw Exception (No broker specified.)
             */
            return;
        }
        $action = $matches[2];
        if (!isset($action))
        {
            /**
             * @todo Throw Exception (No action specified.)
             */
            return;
        }
        $broker = $this->getBroker($brokerName);
        $actionMethod = $action.'Action';
        if ($broker instanceof Balance_Lib_Model_AMQ_Broker_Interface)
        {
            if (method_exists($broker, $actionMethod))
            {
                $broker->setRequest($params);
                $broker->{$actionMethod}();
                return $broker->getResponse();
            }
        }
    }
}
?>
