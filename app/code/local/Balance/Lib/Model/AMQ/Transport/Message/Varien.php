<?php
/**
 * Balance_Lib_Model_AMQ_Transport_Message_Varien class.
 * 
 * @see Varien_Object
 * @see Balance_Lib_Model_AMQ_Transport_Message_Interface
 * @author Derek Li (derek@balanceinternet.com.au)
 */
class Balance_Lib_Model_AMQ_Transport_Message_Varien extends Varien_Object implements Balance_Lib_Model_AMQ_Transport_Message_Interface
{
    protected $_routingKey = null;
    
    public function setRoutingKey($routingKey)
    {
        $this->_routingKey = $routingKey;
    }
    
    public function getRoutingKey()
    {
        return $this->_routingKey;
    }
}
?>
