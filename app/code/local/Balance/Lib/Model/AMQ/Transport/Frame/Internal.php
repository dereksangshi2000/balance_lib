<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Transport_Frame_Internal implements Balance_Lib_Model_AMQ_Transport_Frame_Interface
{
    protected $_server = null;
    
    protected $_serverConfig = array();
    
    public function __construct()
    {
        
    }
    
    public function setServerConfig($serverConfig)
    {
        $this->_serverConfig = $serverConfig;
    }
    
    public function getServerConfig()
    {
        return $this->_serverConfig;
    }
    
    public function setServer($server)
    {
        $this->_server = $server;
    }
    
    public function getServer()
    {
        if (!isset($this->_server))
        {
            $this->_setDefaultServer();
        }
        return $this->_server;
    }
    
    protected function _setDefaultServer()
    {
    }
    
    /**
     * Initialize the connection. 
     */
    public function open()
    {
        
    }
    
    /**
     * Initialize the session.
     * A session is a bidirectional, sequential conversation between two peers
     */
    public function begin()
    {
    }
    
    /**
     * Attach a message.
     * 
     * @param $message The message to attach. 
     * @param mixed $index The index of the message.
     */
    public function attach($message, $index = null)
    {
    }
    
    
    public function transfer()
    {
        $this->getServer()->setConfig($this->getServerConfig());
        return $this->getServer()->run();
    }
    
    /**
     * Send messages over an established link. Messages on a link flow in only one direction.
     */
    public function flow()
    {
        
    }
    
    /**
     * Change message state and settlement.
     * Various reliability guarantees can be enforced this way: at-most-once, at-least-once and exactly-once.
     */
    public function disposition()
    {
        
    }
    
    /**
     * Detach a message.
     * 
     * @param $index The index of the message to detach.
     */
    public function detach($index)
    {
    }
    
    /**
     * Teminate the session.
     */
    public function end()
    {
        
    }
    
    /**
     * Terminate a connection. 
     */
    public function close()
    {
        
    }
}
?>
