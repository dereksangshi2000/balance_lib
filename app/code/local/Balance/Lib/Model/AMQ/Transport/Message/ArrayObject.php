<?php
/**
 * Balance_Lib_Model_AMQ_Transport_Message_ArrayObject class.
 * 
 * @see Balance_Lib_Model_AMQ_Std_ArrayObject
 * @see Balance_Lib_Model_AMQ_Transport_Message_Interface
 * @author Derek Li (derek@balanceinternet.com.au)
 */
class Balance_Lib_Model_AMQ_Transport_Message_ArrayObject extends Balance_Lib_Model_Std_ArrayObject implements Balance_Lib_Model_AMQ_Transport_Message_Interface
{
    protected $_routingKey = null;
    
    public function setRoutingKey($routingKey)
    {
        $this->_routingKey = $routingKey;
    }
    
    public function getRoutingKey()
    {
        return $this->_routingKey;
    }
}
?>
