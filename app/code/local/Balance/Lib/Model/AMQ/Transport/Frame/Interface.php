<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface Balance_Lib_Model_AMQ_Transport_Frame_Interface
{
    /**
     * Initialize the connection. 
     */
    public function open();
    
    /**
     * Initialize the session.
     * A session is a bidirectional, sequential conversation between two peers
     */
    public function begin();
    
    /**
     * Attach a message.
     * 
     * @param $message The message to attach.
     * @param $index OPTIONAL Index of the message.
     */
    public function attach($message, $index = null);
    
    
    public function transfer();
    
    /**
     * Send messages over an established link. Messages on a link flow in only one direction.
     */
    public function flow();
    
    /**
     * Change message state and settlement.
     * Various reliability guarantees can be enforced this way: at-most-once, at-least-once and exactly-once.
     */
    public function disposition();
    
    /**
     * Detach a message.
     * 
     * @param $message The index of the message. 
     */
    public function detach($index);
    
    /**
     * Teminate the session.
     */
    public function end();
    
    /**
     * Terminate a connection. 
     */
    public function close();
}
?>
