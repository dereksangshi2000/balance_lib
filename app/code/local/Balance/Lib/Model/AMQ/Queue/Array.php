<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Balance_Lib_Model_AMQ_Queue_Array extends Balance_Lib_Model_AMQ_Queue_Abstract
{
    protected $_messages = array();
    
    public function getMessages()
    {
        return $this->_messages;
    }
    
    public function push($message)
    {
        array_push($this->_messages, $message);
    }
    
    public function pop()
    {
        return array_pop($this->_messages);
    }
    
    public function shift()
    {
        return array_shift($this->_messages);
    }
    
    public function size()
    {
        if (is_array($this->_messages))
        {
            return count($this->_messages);
        }
    }
}
?>
