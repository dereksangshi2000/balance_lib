<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
interface Balance_Lib_Model_AMQ_Queue_Interface
{
    public function setName($name);
    
    public function getName();
    
    public function push($message);
    
    public function pop();
    
    public function shift();
    
    public function size();
}
?>
