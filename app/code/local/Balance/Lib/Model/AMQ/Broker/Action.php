<?php

/**
 * Balance_Lib_Model_AMQ_Broker_Action class.
 * Standard broker which supports all exchanges.
 * 
 */
class Balance_Lib_Model_AMQ_Broker_Action implements Balance_Lib_Model_AMQ_Broker_Interface
{
    protected $_exchanges = array();
    
    protected $_dispatchers = array();
    
    protected $_request = null;
    
    protected $_response = null;
    
    public function __construct($request = array(), $response = array())
    {
        if (isset($request))
        {
            $this->setRequest($request);
        }
        if (isset($response))
        {
            $this->setResponse($response);
        }
        $this->_init();
    }
    
    protected function _init()
    {
        
    }
    
    public function init()
    {
        
    }
    
    public function setRequest($request)
    {
        $this->_request = $request;
    }
    
    public function getRequest($name = null)
    {
        if (isset($name))
        {
            if (array_key_exists($name, $this->_request))
            {
                return $this->_request[$name];
            }
        }
        else
        {
            return $this->_request;
        }
    }
    
    public function setResponse($response)
    {
        $this->_response = $response;
    }
    
    public function getResponse($name = null)
    {
        if (isset($name))
        {
            if (array_key_exists($name, $this->_response))
            {
                return $this->_response[$name];
            }
        }
        else
        {
            return $this->_response;
        }
    }
    
    public function addExchange(Balance_Lib_Model_AMQ_Exchange_Interface $exchange)
    {
        $this->_exchanges[] = $exchange;
    }
    
    public function getExchanges()
    {
        return $this->_exchanges;
    }
    
    public function addDispatcher($dispatcher)
    {
        $this->_dispatchers[] = $dispatcher;
    }
    
    public function getDispatchers()
    {
        return $this->_dispatchers;
    }
}
?>
